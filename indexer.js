const dirTree = require('directory-tree')
const textract = require('textract')
const fs = require('fs')
const pathAbsoluter = require('path').resolve

function fileTreeToList(tree)
{
    let output = []
    if(tree.type!="directory")
        output.push(tree.path)
    if(tree.children!=undefined)
    {
        for(let i=0; i < tree.children.length; i++)
        {
            let childList = fileTreeToList(tree.children[i])
            if(childList!=undefined)
                output = output.concat(childList)
        }
    }
    return output
}

function htmlWithParams(rootpath, scandate, filepathList, dictpointers)
{
    return `
<!DOCTYPE html>
<html lang="en">
    <style>
        body {
            width: 50%;
            margin-left: 25%;
            margin-right: 25%;
            word-wrap: break-word;
        }

        #searchbox {
            width: 100%;
            margin: 3px;
        }
        #searchbutton {
            margin: 3px;
        }

        #results {
            margin-top: 20px;
        }

        #docuheader {
            font-size: xx-large;
        }

        #scandate {
            float: right;
        }
    </style>
    <body>
        <title id="docutitle">Docudexer - </title>
        <p><sub id="scandate"></sub><b id="docuheader">Docudexer - </b></p>
        <input type="text" id="searchbox">
        <button id="searchbutton" onclick="search()">Search for Documentation</button>

        <ul id="results">
        </ul>
        <p id="count"></p>
    </body>


    <script>
        //FOUR INSRTED VARIABLES -> ROOTPATH, DATA, FILEPATHS, SCANDATE
        const filepaths = ${JSON.stringify(filepathList)}
        const data = ${JSON.stringify(dictpointers)}
        const rootpath = ${JSON.stringify(rootpath)}
        const scandate = ${JSON.stringify(scandate)}

        
        document.getElementById('docutitle').innerHTML+=rootpath
        document.getElementById('docuheader').innerHTML+=rootpath
        document.getElementById('scandate').innerHTML+=scandate
        function search()
        {
            let input = onlyLettersAndSpaces(document.getElementById("searchbox").value.trim()).toLowerCase()

            let searchValues = input.split(" ")
            let searchResults = data['b'+searchValues[0]]
            clearList()
            if(input=="")
                return
            else if (searchResults==undefined)
            {
                listifyFiles([])
                return
            }
            else
                searchResults = JSON.parse(JSON.stringify(searchResults))
 
            for(let i=1; i < searchValues.length; i++) //iterate through each of the searched words
            {
                let temp = data['b'+searchValues[i]] //get every document that contains the next word
                for(let j=0; j < searchResults.length;) //loop through the already known documents
                {
                    let k=0;
                    let found = false;
                    for (; k < temp.length; k++) //loop through documents for the next word
                    {
                        console.log('entering region of k')
                        if(temp[k][0]==searchResults[j][0]) //if a duplicate is found...
                        {
                            searchResults[j][1]+=temp[k][1] //it is kept and the hit value is added.
                            found=true
                            break
                        }
                    }
                    if(found==false) //otherwise the already known document needs to be removed from results list
                        searchResults.splice(j, 1)
                    else
                        j++
                    
                }
            }

            listifyFiles(searchResults)
        }

        function clearList()
        {
            document.getElementById('results').innerHTML=""
            document.getElementById('count').innerHTML=""
        }

        function listifyFiles(filePointers)
        {
            clearList()
            document.getElementById('count').innerHTML=filePointers.length+" files found."
            while(filePointers.length>0)
            {
                let largestIndex = 0
                for(let i=0; i < filePointers.length; i++)
                {
                    if(filePointers[i][1] > filePointers[largestIndex][1])
                        largestIndex=i
                }

                let largest = filePointers.splice(largestIndex, 1)[0]
                let largestName = filepaths[largest[0]]
                document.getElementById('results').innerHTML+="<li><a href='file:"+largestName+"'>"+largestName+"</a> ("+largest[1]+" matches)</li>"
            }
        }

        function onlyLettersAndSpaces(uncleanInput)
        {
            const alphabet = "abcdefghijklmnopqrstuvwxyz0123456789- "
            let output=""
            for(let i=0; i < uncleanInput.length; i++)
            {
                let lowercase = uncleanInput[i].toLowerCase()
                if(alphabet.includes(lowercase))
                    output+=lowercase
                else
                    output+=" "
            }
            return output
        }
    </script>
</html>
`
}

function processFileText(text, dictionary, filepathListIndex)
{
    let wordlist = onlyLettersAndSpaces(text.toLowerCase()).split(' ')

    for(let i=0; i < wordlist.length; i++)
    {
        let word = wordlist[i]
        if(dictionary[`b${word}`]==undefined)
        {
            dictionary[`b${word}`]=[]
        }
        
        let wordCounted=false
        for(let j = 0; j < dictionary[`b${word}`].length; j++)
        {
            if(dictionary[`b${word}`][j][0]==filepathListIndex)
            {
                dictionary[`b${word}`][j][1]++
                wordCounted=true
                break
            }
        }
        if(!wordCounted)
            dictionary[`b${word}`].push([filepathListIndex, 1])
    }
}

function onlyLettersAndSpaces(uncleanInput)
{
    const alphabet = "abcdefghijklmnopqrstuvwxyz0123456789- "
    let output=""
    for(let i=0; i < uncleanInput.length; i++)
    {
        let lowercase = uncleanInput[i].toLowerCase()
        if(alphabet.includes(lowercase))
            output+=lowercase
        else
            output+=" "
    }
    return output
}

function main()
{
    const rootpath = pathAbsoluter(process.argv[2])
    const dictionary = {}
    console.log('Scanning files in '+rootpath)
    const filepathTree = dirTree(rootpath, {attributes:['type']})
    const filepathList = fileTreeToList(filepathTree)
    const mutex={num:1};

    for(let i=0; i < filepathList.length; i++)
    {
        mutex.num++
        textract.fromFileWithPath(filepathList[i], (error, text) => {
            if(error) 
            {
                console.log(mutex.num+' files left.\tFile read error: '+filepathList[i])
            } 
            else {
                processFileText(text, dictionary, i)
                console.log(mutex.num+" files left.")
            }
            mutex.num--

            if(mutex.num == 0)
            {
                let html = htmlWithParams(rootpath, new Date().toDateString(), filepathList, dictionary) //rootpath, scandate, filepathList, dictpointers
                fs.writeFileSync(rootpath+"/docudex.html", html)
                console.log('\nAll done!')
            }
        })
    }
    mutex.num--
}
main()